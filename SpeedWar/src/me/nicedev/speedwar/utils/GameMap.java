package me.nicedev.speedwar.utils;

import me.nicedev.speedwar.SpeedWar;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public abstract class GameMap {

	private String mapName;
	private ConfigurationSection mapSection;
	
	public GameMap(String mapName) {
		this.mapName = mapName;
		this.mapSection = SpeedWar.getPlugin().getConfig().getConfigurationSection("maps." + mapName);
	}
	
	public Location getBlueSpawn() {
		return LocationSerialiser.toLoc(mapSection.getString("bluespawn"));
	}

	public Location getRedSpawn() {
		return LocationSerialiser.toLoc(mapSection.getString("redspawn"));
	}
	
	public String getName() {
		return mapName;
	}
}
