package me.nicedev.speedwar.utils;

import me.nicedev.speedwar.SpeedWar;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public abstract class SpeedWarListener implements Listener {

	public SpeedWar p;
	
	public SpeedWarListener() {
		p = SpeedWar.getPlugin();
		Bukkit.getPluginManager().registerEvents(this, p);
	}
	
}
