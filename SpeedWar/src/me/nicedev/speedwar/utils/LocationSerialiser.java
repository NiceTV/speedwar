package me.nicedev.speedwar.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationSerialiser {

	public static Location toLoc(String string) {
		String[] split = string.split(";");
		
		Location location = null;
		double x = Double.parseDouble(split[0]);
		double y = Double.parseDouble(split[1]);
		double z = Double.parseDouble(split[2]);
		
		if(split.length == 6) {
			location = new Location(Bukkit.getWorld(split[5]), x, y, z, Float.parseFloat(split[3]), Float.parseFloat(split[4]));
		} else {
			location = new Location(Bukkit.getWorld(split[3]), x, y, z);
		}
		
		return location;
	}
	
	public static String locToString(Location loc, boolean yp) {
		return loc.getBlockX() + 
				";" + loc.getBlockY() + 
				";" + loc.getBlockZ() + 
				";" + (yp ? loc.getYaw() + ";" + loc.getPitch() + 
						";" : "") +
				loc.getWorld().getName();
	}
	
}
