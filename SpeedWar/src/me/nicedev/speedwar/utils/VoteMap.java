package me.nicedev.speedwar.utils;

import me.nicedev.speedwar.SpeedWar;

import org.bukkit.entity.Player;

public class VoteMap extends GameMap {
	
	private int votes = 0;
	private SpeedWar p;
	private int number;
	
	public VoteMap(String mapName, int number) {
		super(mapName);
		
		this.p = SpeedWar.getPlugin();
		this.number = number;
	}
	
	public void addVote(Player player) {
		if(p.currentMap != null) {
			player.sendMessage(p.prefix + "�cDas Voting ist bereits zuende!");
			return;
		}
		
		if(p.voted.contains(player.getUniqueId())) {
			player.sendMessage(p.prefix + "�cDu hast bereits gevotet!");
			return;
		}
		
		p.voted.add(player.getUniqueId());
		player.sendMessage(p.prefix + "�aDie Map �6" + getName() + " �ahat nun �6" + votes + " �aVotes!");
		votes++;
		p.voteScoreboard.setLine(number - 1, "�c" + getName(), votes);
		p.voteMaps.put(new Integer(number), this);
	}
	
	public int getVotes() {
		return votes;
	}

}
