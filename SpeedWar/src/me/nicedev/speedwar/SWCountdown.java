package me.nicedev.speedwar;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;
import java.util.UUID;

import me.nicedev.speedwar.utils.GameMap;
import me.nicedev.speedwar.utils.ItemUtils;
import me.nicedev.speedwar.utils.VoteMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class SWCountdown extends BukkitRunnable {

	private SpeedWar p;
	
	public SWCountdown() {
		p = SpeedWar.getPlugin();
		this.runTaskTimer(p, 0L, 20L);
	}
	
	public void run() {
		if(p.lobbyCountdown != -1) {
			if(p.lobbyCountdown != 0) {
				if(p.lobbyCountdown % 10 == 0 || p.lobbyCountdown <= 5) {
					Bukkit.broadcastMessage(p.prefix + "�aDas Spiel beginnt in �6" + p.lobbyCountdown + " �aSekunden!");
				}
				
				if(p.lobbyCountdown == 10) {
					endVote();
				}
				
				p.lobbyCountdown--;
			} else {
				if(Bukkit.getOnlinePlayers().length < p.minPlayers && !p.started) {
					p.currentMap = null;
					if(Bukkit.getOnlinePlayers().length == 0) {
						p.lobbyCountdown = -1;
						return;
					}
					
					p.lobbyCountdown = p.lobbyMaxTime;
					Bukkit.broadcastMessage(p.prefix + "�cWarten auf Spieler...");
				} else if(p.started) {
					p.started = false;
					endVote();
				}
				
				List<UUID> redCopy = p.redQueue;
				List<UUID> blueCopy = p.blueQueue;
				
				// 0 = Team Blau // 1 = Team Rot
				int currentTeam = 0;
				int counter = 0;
				int players = Bukkit.getOnlinePlayers().length;
				
				int maxBlue = (players / 2) + (players % 2 == 1 ? 1 : 0);
				int maxRed = players - maxBlue;
				
				while(counter != players) {
					if(currentTeam == 0
							&& maxBlue != p.blueTeam.size()
							&& blueCopy.size() >= 1) {
						UUID firstInQueue = blueCopy.get(0);
						p.blueTeam.add(firstInQueue);
						blueCopy.remove(0);
					} else if(currentTeam == 1
							&& maxRed != p.redTeam.size()
							&& redCopy.size() >= 1) {
						UUID firstInQueue = redCopy.get(0);
						p.redTeam.add(firstInQueue);
						redCopy.remove(0);
					} else {
						List<UUID> available = new ArrayList<UUID>();
						
						for(Player player : Bukkit.getOnlinePlayers()) {
							if(p.redTeam.contains(player.getUniqueId()) || p.blueTeam.contains(player.getUniqueId())) continue;
							
							available.add(player.getUniqueId());
						}
						
						UUID randomUUID = available.get(new Random().nextInt(available.size()));
						
						switch(currentTeam) {
						case 0:
							p.blueTeam.add(randomUUID);
							break;
						case 1:
							p.redTeam.add(randomUUID);
							break;
						}
					}
						
					counter++;
					currentTeam = (currentTeam == 0) ? 1 : 0;
				}
				
				p.lives.put("red", p.getConfig().getInt("maxlives"));
				p.lives.put("blue", p.getConfig().getInt("maxlives"));
				
				Bukkit.broadcastMessage(p.prefix + "�aDas Spiel beginnt �6jetzt�a!");
				
				for(Player player : Bukkit.getOnlinePlayers()) {
					player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, p.getConfig().getInt("speedpower")));
					p.points.put(player.getUniqueId(), 0);
					
					String team = "";
					if(p.redTeam.contains(player.getUniqueId())) {
						p.ingameScoreboard.getTeam("red").addPlayer(player);
						team = "�9blauen";
						player.teleport(p.currentMap.getRedSpawn());
					} else {
						p.ingameScoreboard.getTeam("blue").addPlayer(player);
						team = "�croten";
						player.teleport(p.currentMap.getBlueSpawn());
					}
					
					player.getInventory().setItem(0, new ItemStack(Material.STONE_SWORD));
					player.getInventory().setItem(7, ItemUtils.setDisplayName(new ItemStack(Material.WOOL, p.blueTeam.size(), (byte) 11), 
							"�9Leben von Team Blau"));
					player.getInventory().setItem(8, ItemUtils.setDisplayName(new ItemStack(Material.WOOL, p.redTeam.size(), (byte) 14), 
							"�cLeben von Team Rot"));
					
					player.sendMessage(p.prefix + "�bDu bist im " + team + " �bTeam!");
				}
				
				p.lobbyCountdown--;
			}
		}
	}
	
	private void endVote() {
		int mostVotes = -1;
		GameMap bestMap = null;
		
		for(Entry<Integer, VoteMap> voteMapEntry : p.voteMaps.entrySet()) {
			if(voteMapEntry.getValue().getVotes() > mostVotes) {
				bestMap = voteMapEntry.getValue();
				mostVotes = voteMapEntry.getValue().getVotes();
			}
		}
		
		p.currentMap = bestMap;
		Bukkit.broadcastMessage(p.prefix + "�bDie Map �6" + bestMap.getName() + " �bhat das Voting gewonnen!");
	}
	
}
