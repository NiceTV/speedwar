package me.nicedev.speedwar.listeners;

import java.util.UUID;

import me.nicedev.speedwar.utils.SpeedWarListener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class ClickListener extends SpeedWarListener {

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if(!(e.getWhoClicked() instanceof Player)) return;
		Player player = (Player) e.getWhoClicked();
		ItemStack item = e.getCurrentItem();
		
		if(item == null || item.getType() == Material.AIR) return;
		if(item.getType() == Material.STONE_SWORD 
				|| item.getType() == Material.WOOL
				|| item.getType().name().startsWith("DIAMOND_") 
				|| item.getType().name().startsWith("IRON_")) {
			e.setCurrentItem(null);
			e.setCancelled(true);
			return;
		}
		
		if(!e.getInventory().getTitle().equals("�cShop")) return;
		e.setCancelled(true);
		e.setCurrentItem(null);
		
		if(item.getType().name().endsWith("_CHESTPLATE")) {
			if(item.getType().name().startsWith("DIAMOND_")) {
				if(player.getInventory().getChestplate() != null 
						&& player.getInventory().getChestplate().getType().name().startsWith("DIAMOND_")) {
					player.sendMessage(p.prefix + "�cDu hast bereits eine Diamant-R�stung!");
					player.closeInventory();
					return;
				}	
				
				if(getPoints(player.getUniqueId()) < p.prices.get("diamondarmor")) {
					player.sendMessage(p.prefix + "�cDu hast nicht genug Punkte!");
					return;
				}
				
				player.getInventory().setHelmet(new ItemStack(Material.DIAMOND_HELMET));
				player.getInventory().setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
				player.getInventory().setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
				player.getInventory().setBoots(new ItemStack(Material.DIAMOND_BOOTS));
				
				removePoints(player.getUniqueId(), p.prices.get("diamondarmor"));
				player.sendMessage(p.prefix + "�aEs wurde eine Diamant-R�stung gekauft!");
				player.sendMessage(p.prefix + "�bDu hast nun �6" + getPoints(player.getUniqueId()) + " �bPunkte!");
				player.closeInventory();
				return;
			}
			
			if(item.getType().name().startsWith("IRON_")) {
				if(player.getInventory().getChestplate() != null 
						&& player.getInventory().getChestplate().getType().name().startsWith("IRON_")) {
					player.sendMessage(p.prefix + "�cDu hast bereits eine Eisen-R�stung!");
					player.closeInventory();
					return;
				}	
				
				if(getPoints(player.getUniqueId()) < p.prices.get("ironarmor")) {
					player.sendMessage(p.prefix + "�cDu hast nicht genug Punkte!");
					return;
				}
				
				player.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET));
				player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
				player.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS));
				player.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
				
				removePoints(player.getUniqueId(), p.prices.get("ironarmor"));
				player.sendMessage(p.prefix + "�aEs wurde eine Eisen-R�stung gekauft!");
				player.sendMessage(p.prefix + "�bDu hast nun �6" + getPoints(player.getUniqueId()) + " �bPunkte!");
				player.closeInventory();
				return;
			}
		}
	}
	
	public int getPoints(UUID uuid) {
		return p.points.get(uuid);
	}
	
	public void removePoints(UUID uuid, int points) {
		p.points.put(uuid, p.points.get(uuid) - points);
	}
	
}
