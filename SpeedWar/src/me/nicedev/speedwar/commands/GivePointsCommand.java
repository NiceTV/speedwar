package me.nicedev.speedwar.commands;

import java.util.Arrays;

import me.nicedev.speedwar.SpeedWar.GameState;
import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GivePointsCommand extends BaseCommand {

	public GivePointsCommand() {
		super("givepoints", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!sender.hasPermission("speedwar.givepoints")) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		if(args.length < 2) {
			sender.sendMessage(p.prefix + "�cVerwendung: /givepoints <Name> <Points>");
			return;
		}
		
		if(!p.gameplay) {
			sender.sendMessage(p.prefix + "�cAktiviere erst das Gameplay!");
			return;
		}
		
		if(p.state != GameState.INGAME) {
			sender.sendMessage(p.prefix + "�cDas Spiel ist nicht InGame!");
			return;
		}
		
		if(Bukkit.getPlayer(args[0]) == null) {
			sender.sendMessage(p.prefix + "�cDieser Spieler ist nicht online!");
			return;
		}
		
		int points = 0;
		try {
			points = Integer.parseInt(args[1]);
		} catch(NumberFormatException e) {
			sender.sendMessage(p.prefix + "�cUng�ltige Zahl!");
			return;
		}
		
		if(points < 1) {
			sender.sendMessage(p.prefix + "�cGebe mindestens 1 Punkt an!");
			return;
		}

		p.points.put(Bukkit.getPlayer(args[0]).getUniqueId(), points + p.points.get(Bukkit.getPlayer(args[0]).getUniqueId()));
		sender.sendMessage(p.prefix + "�aDu hast diesem Spieler �6" + points + " �aPunkte gegeben!");
	}

}
