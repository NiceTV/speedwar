package me.nicedev.speedwar.commands;

import java.util.Arrays;

import me.nicedev.speedwar.utils.LocationSerialiser;
import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawnRedCommand extends BaseCommand {

	public SetSpawnRedCommand() {
		super("setspawnred", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!sender.hasPermission("speedwar.admin")) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		if(args.length < 1) {
			sender.sendMessage(p.prefix + "�cVerwendung: /setspawnred <Map>");
			return;
		}
		
		if(p.gameplay) {
			sender.sendMessage(p.prefix + "�cDeaktiviere erst das Gameplay!");
			return;
		}
		
		if(!p.getConfig().contains("maps." + args[0])) {
			sender.sendMessage(p.prefix + "�cDiese Map existiert nicht!");
			return;
		}
		
		p.getConfig().set("maps." + args[0] + ".redspawn", LocationSerialiser.locToString(((Player) sender).getLocation(), true));
		p.saveConfig();
		
		sender.sendMessage(p.prefix + "�aDie Base des roten Teams wurde gesetzt!");
	}

}
