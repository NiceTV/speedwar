package me.nicedev.speedwar.commands;

import java.util.Arrays;

import me.nicedev.speedwar.utils.LocationSerialiser;
import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetLobbySpawnCommand extends BaseCommand {

	public SetLobbySpawnCommand() {
		super("setlobbyspawn", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!sender.hasPermission("speedwar.admin")) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		p.getConfig().set("lobbyspawn", LocationSerialiser.locToString(((Player) sender).getLocation(), true));
		p.saveConfig();
		
		sender.sendMessage(p.prefix + "�aDer Lobbyspawn wurde erfolgreich gesetzt!");
	}

}
