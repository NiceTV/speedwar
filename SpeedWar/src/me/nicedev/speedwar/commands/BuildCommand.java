package me.nicedev.speedwar.commands;

import java.util.Arrays;

import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BuildCommand extends BaseCommand {

	public BuildCommand() {
		super("build", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!sender.hasPermission("speedwar.admin")) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		if(args.length < 1) {
			sender.sendMessage(p.prefix + "�cVerwendung: /build on/off");
			return;
		}
		
		boolean enabled = args[0].equalsIgnoreCase("on") ? true : false;
		
		p.buildMode = enabled;
		sender.sendMessage(p.prefix + "�aBau-Modus wurde " + (enabled ? "�a" : "�cde") + "aktiviert!");
	}

}
