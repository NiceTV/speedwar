package me.nicedev.speedwar.commands;

import java.util.Arrays;

import me.nicedev.speedwar.SpeedWar.GameState;
import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ShopCommand extends BaseCommand {

	public ShopCommand() {
		super("b", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!p.gameplay) {
			sender.sendMessage(p.prefix + "�cAktiviere erst das Gameplay!");
			return;
		}
		
		if(p.state != GameState.INGAME) {
			sender.sendMessage(p.prefix + "�cDas Spiel ist nicht InGame!");
			return;
		}

		((Player) sender).openInventory(p.shopInventory);
	}

}
