package me.nicedev.speedwar.commands;

import java.io.File;
import java.util.Arrays;

import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class MapCommand extends BaseCommand {

	public MapCommand() {
		super("map", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!sender.hasPermission("speedwar.admin")) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		if(args[0].equalsIgnoreCase("add")) {
			if(args.length < 2) {
				sender.sendMessage(p.prefix + "�cVerwendung: /map add <Name>");
				return;
			}
			
			if(p.getConfig().contains("maps." + args[1])) {
				sender.sendMessage(p.prefix + "�cDiese Map existiert bereits!");
				return;
			}
			
			if(!new File(p.getServer().getWorldContainer(), args[1]).exists()) {
				sender.sendMessage(p.prefix + "�cDiese Welt existiert nicht!");
				return;
			}
			
			ConfigurationSection cs = p.getConfig().createSection("maps." + args[1]);
			cs.set("bluespawn", "none");
			cs.set("redspawn", "none");
			
			p.saveConfig();
			
			Bukkit.createWorld(new WorldCreator(args[1]));
			sender.sendMessage(p.prefix + "�aMap wurde erfolgreich erstellt und geladen!");
			return;
		}
		
		if(args[0].equalsIgnoreCase("remove")) {
			if(args.length < 2) {
				sender.sendMessage(p.prefix + "�cVerwendung: /map remove <Name>");
				return;
			}
			
			if(!p.getConfig().contains("maps." + args[1])) {
				sender.sendMessage(p.prefix + "�cDiese Map existiert nicht!");
				return;
			}
			
			p.getConfig().set("maps." + args[1], null);
			p.saveConfig();
			
			sender.sendMessage(p.prefix + "�aMap wurde gel�scht!");
			return;
		}
		
		if(args[0].equalsIgnoreCase("list")) {
			sender.sendMessage(p.prefix + "�aErstellte Maps:");
			
			for(String key : p.getConfig().getConfigurationSection("maps").getKeys(false)) {
				sender.sendMessage(" �c- " + key);
			}
			
			return;
		}
		
		sender.sendMessage(p.prefix + "�aSpeedWar-Hilfemen� \n"
				+ " �c/map add <Name> �7-> �aF�gt eine Map hinzu \n"
				+ " �c/map remove <Name>  �7-> �aL�scht eine Map \n"
				+ " �c/map list �7-> �aZeigt alle Maps an");
	}

}
