package me.nicedev.speedwar.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigUtils {

	private File configFile;
	private FileConfiguration config;
	
	public ConfigUtils() {
		this.configFile = new File("plugins/SpeedWar/config.yml");
		this.config = YamlConfiguration.loadConfiguration(configFile);

		if(!configFile.getParentFile().exists()) {
			configFile.getParentFile().mkdir();
		}
		
		if(!configFile.exists()) {
			try {
				configFile.createNewFile();
			} catch(IOException e) {
				System.err.println("[SW] Error: " + e.getMessage());
			}
		}
		
		/* Optionen */
		config.options().copyDefaults(true);
		
		/* Main-Sachen */
		config.addDefault("gameplay", false);
		config.addDefault("lobbyspawn", "0.0;0.0;0.0;0.0;0.0;world");
		config.addDefault("lobbyCountdown", 60);
		config.addDefault("minplayers", 4);
		config.addDefault("maxplayers", 30);
		config.addDefault("speedpower", 2);
		config.addDefault("maxlives", 30);
		
		config.addDefault("prices.snowball", 2);
		config.addDefault("prices.tnts", 2);
		config.addDefault("prices.enderpearl", 3);
		config.addDefault("prices.ironarmor", 3);
		config.addDefault("prices.diamondarmor", 4);
		config.addDefault("prices.magmaball", 5);
		
		/* Map-Section */
		if(!config.contains("maps")) {
			config.createSection("maps");
		}
		
		saveConfig();
	}
	
	public void saveConfig() {
		try {
			config.save(configFile);
		} catch(IOException e) {
			System.err.println("[SW] Error: " + e.getMessage());
		}
	}
	
	public File getConfigFile() {
		return configFile;
	}
	
	public FileConfiguration getConfig() {
		return config;
	}
	
}
