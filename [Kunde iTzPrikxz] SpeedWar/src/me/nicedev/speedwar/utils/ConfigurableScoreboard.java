
package me.nicedev.speedwar.utils;
 
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
 
public class ConfigurableScoreboard
{
  private static final String[] EMPTY_LINES = {
    "�0", "�1", "�2", "�3", "�4", "�5", "�6", "�7", "�8", "�9", "�a", "�b", "�c", "�d" };
  @SuppressWarnings("unused")
private static final String STRING_EMPTY = "";
  @SuppressWarnings("unused")
private static final String STRING_SPACE = " ";
  public static final int MAX_LINE = 13;
  public static final int LINES = 14;
  public static final int SPLIT_PREFIX = 0;
  public static final int SPLIT_NAME = 1;
  public static final int SPLIT_SUFFIX = 2;
  @SuppressWarnings("unused")
private static final int SPLIT_LENGTH = 3;
  @SuppressWarnings("unused")
private static final String OBJECTIVE_NAME = "title";
  private String[][] lines = new String[14][3];
  private Scoreboard scoreboard;
  public boolean sorted;
  public String title;
  
  public ConfigurableScoreboard(String title, boolean sorted)
  {
    this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    this.scoreboard.registerNewObjective("title", "dummy");
    setTitle(title);
    for (int line = 0; line < 14; line++)
      clearLine(line);
    this.sorted = sorted;
  }
 
  public String getLine(int line)
  {
    String[] split = this.lines[line];
    String content = split[0] + split[1] + split[2];
    return content.equals("") ? null : content;
  }
  
  public int getScore(int line) {
	  String[] split = this.lines[line];
	  return this.scoreboard.getObjective("title").getScore(Bukkit.getOfflinePlayer(split[1])).getScore();
  }
 
  public void setLine(int line, String content, Alignment alignment, int score) {
    int offset = 0;
    if (alignment == Alignment.RIGHT) {
      offset = 48 - content.length();
    } else if (alignment == Alignment.CENTER) {
      offset = (48 - content.length()) / 2;
      offset += (content.length() - ChatColor.stripColor(content).length()) / 2;
    }
    String add = "";
    for (int i = 0; i < offset; i++) {
      add = add + " ";
      if ((i == 16) && (i + 4 < offset)) {
        add = add + EMPTY_LINES[line] + ChatColor.RESET;
        i += 4;
      }
    }
    content = add + content;
    setLine(line, content, score);
  }
 
  @SuppressWarnings("deprecation")
public void setLine(int line, String content, int score) {
    if (line >= 14) {
      line = 13;
    }
    if (content == null) {
      clearLine(line);
      return;
    }
    if (content.length() > 48) {
      content = content.substring(0, 48);
    }
    String current = getLine(line);
    String[] split = new String[3];
    Arrays.fill(split, "");
    if (content.length() > 16) {
      split[0] = content.substring(0, 16);
      if (content.length() > 32) {
        split[1] = content.substring(16, 32);
        split[2] = content.substring(32, content.length());
      } else {
        split[1] = content.substring(16, content.length());
      }
    } else {
      split[1] = content;
    }
 
    clearLine(line);
 
    for (int i = 0; i < 14; i++) {
      if (this.lines[i][1].equals(split[1])) {
        if (split[1].length() > 12) {
          String appendix = split[1].substring(12, split[1].length());
          split[1] = split[1].substring(0, 12);
          split[2] = (appendix + split[2]);
          if (split[2].length() > 16) {
            split[2] = split[2].substring(0, 16);
          }
        }
        split[1] = (EMPTY_LINES[line] + ChatColor.getLastColors(split[1]) + split[1]);
        break;
      }
    }
 
    OfflinePlayer fakePlayer = Bukkit.getOfflinePlayer(split[1]);
 
    Objective obj = this.scoreboard.getObjective("title");
 
    obj.setDisplaySlot(DisplaySlot.SIDEBAR);
    obj.getScore(fakePlayer).setScore(!this.sorted ? (14 - line) : score);
 
    for (int i = 0; i < line; i++) {
      if (getLine(i) == null) {
        setLine(i, EMPTY_LINES[i], score);
      }
    }
    this.lines[line] = split;
 
    Team team = this.scoreboard.getTeam(fakePlayer.getName());
    if (team == null) {
      team = this.scoreboard.registerNewTeam(fakePlayer.getName());
    }
    team.setPrefix(split[0]);
    team.setSuffix(split[2]);
    team.addPlayer(fakePlayer);
  }
 
  @SuppressWarnings("deprecation")
  public void clearLine(int line) {
    String[] split = this.lines[line];
    if ((split[1] != null) && (!split[1].equals(""))) {
      this.scoreboard.resetScores(Bukkit.getOfflinePlayer(split[1]));
    }
    Arrays.fill(split, "");
    this.lines[line] = split;
  }
  
  public void setTitle(String title) {
    if (title.length() > 32) {
      title = title.substring(0, 32);
    }
    this.scoreboard.getObjective("title").setDisplayName(title);
  }
 
  public int getLineFromString(String string) {
	  for(int i = 0; i < this.lines.length; i++) {
		  if(getLine(i) == null) continue;
		  if(getLine(i).equalsIgnoreCase(string)) {
			  return i;
		  }
	  }
	  
	  return 0;
  }
  
  public void displayScoreboard(Player player) {
    player.setScoreboard(this.scoreboard);
  }
  
  public Scoreboard getScoreboard() {
	  return this.scoreboard;
  }
  
  public static enum Alignment
  {
    LEFT,
    RIGHT,
    CENTER;
  }
}