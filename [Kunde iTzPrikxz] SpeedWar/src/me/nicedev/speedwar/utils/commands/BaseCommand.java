package me.nicedev.speedwar.utils.commands;

import java.util.List;

import me.nicedev.speedwar.SpeedWar;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public abstract class BaseCommand implements CommandExecutor {

	private String cmd;
	private List<Object> args;
	public SpeedWar p = SpeedWar.getPlugin();
	
	public BaseCommand(String cmd, List<Object> list) {
		this.cmd = cmd;
		this.args = list;
		
		SpeedWar.getPlugin().getCommand(cmd).setExecutor(this);
	}
	
	public String getCommand() {
		return this.cmd;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(args.length != 0) {
			for(Object argObject : this.args) {
				ArgumentCommand arg = (ArgumentCommand) argObject;
				if(args[0].equalsIgnoreCase(arg.getArgument())) {
					arg.executeCommand(sender, args);
					return true;
				}
			}
		}

		execute(sender, args);
		return true;
	}
	
	public abstract void execute(CommandSender sender, String[] args);	
}
