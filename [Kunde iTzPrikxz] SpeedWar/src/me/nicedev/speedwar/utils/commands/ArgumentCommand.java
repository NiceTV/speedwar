package me.nicedev.speedwar.utils.commands;

import me.nicedev.speedwar.SpeedWar;

import org.bukkit.command.CommandSender;

public abstract class ArgumentCommand {

	private String arg;
	private String perm;
	public SpeedWar p = SpeedWar.getPlugin();
	
	public ArgumentCommand(String arg, String perm) {
		this.arg = arg;
		this.perm = perm;
	}

	public String getArgument() {
		return this.arg;
	}
	
	public abstract void execute(CommandSender sender, String[] args);
	
	public void executeCommand(CommandSender sender, String[] args) {
		if(!sender.hasPermission(this.perm)) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		String[] newArgs = new String[args.length - 1];
		int counter = 0;
		
		for(String arg : args) {
			if(arg.equals(args[0])) {
				continue;
			}
			
			newArgs[counter] = args[counter + 1];
			counter++;
		}
		
		execute(sender, newArgs);
	}
}
