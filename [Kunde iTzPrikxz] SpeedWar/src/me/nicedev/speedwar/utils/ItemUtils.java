package me.nicedev.speedwar.utils;

import java.util.List;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtils {

	public static ItemStack setDisplayName(ItemStack itemstack, String name) {
		ItemMeta im = itemstack.getItemMeta();
		im.setDisplayName(name);
		itemstack.setItemMeta(im);
		
		return itemstack;
	}
	
	public static ItemStack setLore(ItemStack itemstack, List<String> lore) {
		ItemMeta im = itemstack.getItemMeta();
		im.setLore(lore);
		itemstack.setItemMeta(im);
		
		return itemstack;
	}
	
}
