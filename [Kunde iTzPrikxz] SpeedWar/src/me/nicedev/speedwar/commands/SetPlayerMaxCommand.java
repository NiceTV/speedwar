package me.nicedev.speedwar.commands;

import java.util.Arrays;

import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetPlayerMaxCommand extends BaseCommand {

	public SetPlayerMaxCommand() {
		super("setplayermax", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!sender.hasPermission("speedwar.admin")) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		if(args.length < 1) {
			sender.sendMessage(p.prefix + "�cVerwendung: /setplayermax <Anzahl>");
			return;
		}
		
		if(p.gameplay) {
			sender.sendMessage(p.prefix + "�cDeaktiviere erst das Gameplay!");
			return;
		}
		
		int maxplayers = 0;
		try {
			maxplayers = Integer.parseInt(args[0]);
		} catch(Exception ex) {
			sender.sendMessage(p.prefix + "�cUng�ltige Zahl!");
			return;
		}
		
		if(maxplayers < 2) {
			sender.sendMessage(p.prefix + "�cUng�ltige Zahl!");
			return;
		}

		p.getConfig().set("maxplayers", maxplayers);
		p.saveConfig();
		
		sender.sendMessage(p.prefix + "�aMaximum-Spieleranzahl wurde erfolgreich gesetzt!");
	}

}
