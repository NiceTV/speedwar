package me.nicedev.speedwar.commands;

import java.util.Arrays;

import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetPlayerMinCommand extends BaseCommand {

	public SetPlayerMinCommand() {
		super("setplayermin", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!sender.hasPermission("speedwar.admin")) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		if(args.length < 1) {
			sender.sendMessage(p.prefix + "�cVerwendung: /setplayermin <Anzahl>");
			return;
		}
		
		if(p.gameplay) {
			sender.sendMessage(p.prefix + "�cDeaktiviere erst das Gameplay!");
			return;
		}
		
		int minplayers = 0;
		try {
			minplayers = Integer.parseInt(args[0]);
		} catch(Exception ex) {
			sender.sendMessage(p.prefix + "�cUng�ltige Zahl!");
			return;
		}
		
		if(minplayers <= 1) {
			sender.sendMessage(p.prefix + "�cUng�ltige Zahl!");
			return;
		}

		p.getConfig().set("minplayers", minplayers);
		p.saveConfig();
		
		sender.sendMessage(p.prefix + "�aMindest-Spieleranzahl wurde erfolgreich gesetzt!");
	}

}
