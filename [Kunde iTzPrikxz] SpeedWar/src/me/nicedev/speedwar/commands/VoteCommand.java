package me.nicedev.speedwar.commands;

import java.util.Arrays;
import java.util.Map.Entry;

import me.nicedev.speedwar.utils.VoteMap;
import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VoteCommand extends BaseCommand {

	public VoteCommand() {
		super("vote", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;

		if(!p.gameplay) {
			sender.sendMessage(p.prefix + "�cAktiviere erst das Gameplay!");
			return;
		}
		
		if(args.length < 1) {
			sender.sendMessage(p.prefix + "�cVerwendung: /vote <Map/Nummer>");
			return;
		}
		
		boolean numberic = true;
		int number = 0;
		try {
			number = Integer.parseInt(args[0]);
		} catch(NumberFormatException e) {
			numberic = false;
		}
		
		if(numberic) {
			if(!p.voteMaps.containsKey(new Integer(number))) {
				sender.sendMessage(p.prefix + "�cUng�ltige Map!");
				return;
			}
			
			p.voteMaps.get(new Integer(number)).addVote(((Player) sender));
			return;
		}
		
		for(Entry<Integer, VoteMap> voteMapEntry : p.voteMaps.entrySet()) {
			if(voteMapEntry.getValue().getName().equalsIgnoreCase(args[0])) {
				voteMapEntry.getValue().addVote(((Player) sender));
				return;
			}
		}
		
		sender.sendMessage(p.prefix + "�cUng�ltige Map!");
	}

}
