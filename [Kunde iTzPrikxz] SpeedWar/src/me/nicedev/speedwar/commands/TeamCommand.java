package me.nicedev.speedwar.commands;

import java.util.Arrays;
import java.util.UUID;

import me.nicedev.speedwar.SpeedWar.GameState;
import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeamCommand extends BaseCommand {

	public TeamCommand() {
		super("team", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;

		if(!p.gameplay) {
			sender.sendMessage(p.prefix + "�cAktiviere erst das Gameplay!");
			return;
		}
		
		if(args.length < 1) {
			sender.sendMessage(p.prefix + "�cVerwendung: /team �1blue�7/�4red");
			return;
		}
		
		if(p.state != GameState.WAITING) {
			sender.sendMessage(p.prefix + "�cDas Spiel ist bereits gestartet!");
			return;
		}

		if(!args[0].equalsIgnoreCase("blue") && !args[0].equalsIgnoreCase("red")) {
			sender.sendMessage(p.prefix + "�cUng�ltiges Team!");
			return;
		}
		
		UUID uuid = ((Player) sender).getUniqueId();
		
		if(p.redQueue.contains(uuid)) {
			p.redQueue.remove(uuid);
		}
		
		if(p.blueQueue.contains(uuid)) {
			p.blueQueue.remove(uuid);
		}
		
		String team = args[0].equalsIgnoreCase("red") ? "�croten" : "�bblauen";
		
		if(args[0].equalsIgnoreCase("blue")) {
			p.blueQueue.add(uuid);
		} else {
			p.redQueue.add(uuid);
		}
		
		sender.sendMessage(p.prefix + "�aDu bist der Warteschlange des " + team + " �aTeams beigetreten!");
	}

}
