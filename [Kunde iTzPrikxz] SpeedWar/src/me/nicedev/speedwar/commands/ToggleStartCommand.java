package me.nicedev.speedwar.commands;

import java.util.Arrays;

import me.nicedev.speedwar.SpeedWar.GameState;
import me.nicedev.speedwar.utils.commands.BaseCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleStartCommand extends BaseCommand {

	public ToggleStartCommand() {
		super("togglestart", Arrays.asList());
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!(sender instanceof Player)) return;
		
		if(!sender.hasPermission("speedwar.start")) {
			sender.sendMessage(p.prefix + "�cKeine Rechte!");
			return;
		}
		
		if(!p.gameplay) {
			sender.sendMessage(p.prefix + "�cAktiviere erst das Gameplay!");
			return;
		}
		
		if(p.state != GameState.WAITING) {
			sender.sendMessage(p.prefix + "�cDas Spiel ist bereits gestartet!");
			return;
		}

		if(Bukkit.getOnlinePlayers().length < 2) {
			sender.sendMessage(p.prefix + "�cEs m�ssen mindestens 2 Spieler online sein!");
			return;
		}
		
		p.started = true;
		p.lobbyCountdown = 0;
		
		sender.sendMessage(p.prefix + "�aDas Spiel wurde gestartet!");
	}

}
