package me.nicedev.speedwar.listeners;

import me.nicedev.speedwar.utils.SpeedWarListener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class ClickListener extends SpeedWarListener {

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if(!(e.getWhoClicked() instanceof Player)) return;
		Player player = (Player) e.getWhoClicked();
		ItemStack item = e.getCurrentItem();
		
		if(item.getType() == Material.STONE_SWORD || item.getType() == Material.WOOL) {
			e.setCurrentItem(null);
			e.setCancelled(true);
			return;
		}
		
		if(!e.getInventory().getTitle().equals("�cShop")) return;
		e.setCurrentItem(null);
		
	}
	
}
