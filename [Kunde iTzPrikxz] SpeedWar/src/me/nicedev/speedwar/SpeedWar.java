package me.nicedev.speedwar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import me.nicedev.speedwar.utils.ConfigUtils;
import me.nicedev.speedwar.utils.ConfigurableScoreboard;
import me.nicedev.speedwar.utils.GameMap;
import me.nicedev.speedwar.utils.ItemUtils;
import me.nicedev.speedwar.utils.VoteMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

public class SpeedWar extends JavaPlugin 
{

	public String prefix = "�7[�6SpeedWar�7] �r";
	public ConfigUtils cfgUtils;
	
	public boolean buildMode = false;
	public boolean gameplay;
	
	public boolean started = false;
	public int lobbyCountdown = -1;
	public GameState state = GameState.WAITING;
	public ConfigurableScoreboard voteScoreboard;
	
	public GameMap currentMap;
	public Scoreboard ingameScoreboard;
	
	public Inventory shopInventory;
	public Map<String, Integer> prices = new HashMap<String, Integer>();
	public Map<UUID, Integer> points = new HashMap<UUID, Integer>();
	
	public List<UUID> redQueue = new ArrayList<UUID>();
	public List<UUID> blueQueue = new ArrayList<UUID>();
	
	public List<UUID> redTeam = new ArrayList<UUID>();
	public List<UUID> blueTeam = new ArrayList<UUID>();
	public Map<String, Integer> lives = new HashMap<String, Integer>();
	
	public List<UUID> voted = new ArrayList<UUID>();
	public Map<Integer, VoteMap> voteMaps = new HashMap<Integer, VoteMap>();
	
	public int minPlayers;
	public int maxPlayers;
	public int lobbyMaxTime;
	
	public void onEnable() {
		instance = this;
		cfgUtils = new ConfigUtils();
		
		this.minPlayers = getConfig().getInt("minplayers");
		this.maxPlayers = getConfig().getInt("maxplayers");
		this.lobbyMaxTime = getConfig().getInt("lobbyCountdown");
		this.gameplay = getConfig().getBoolean("gameplay");
		
		shopInventory = Bukkit.createInventory(null, 9, "�cShop");
		
		ConfigurationSection prices = getConfig().getConfigurationSection("prices");
		this.prices.put("snowball", prices.getInt("snowball"));
		this.prices.put("tnts", prices.getInt("tnts"));
		this.prices.put("enderpearl", prices.getInt("enderpearl"));
		this.prices.put("ironarmor", prices.getInt("ironarmor"));
		this.prices.put("diamondarmor", prices.getInt("diamondarmor"));
		this.prices.put("magmaball", prices.getInt("magmaball"));
		
		ItemStack explosion = ItemUtils.setDisplayName(new ItemStack(Material.SNOW_BALL), "�bLeichte Explosion");
		explosion = ItemUtils.setLore(explosion, Arrays.asList("�aErzeugt eine", "�6 leichte Explosion", "�a" 
				+ this.prices.get("snowball") + " Punkte"));
		shopInventory.addItem(explosion);
		
		ItemStack bombs = ItemUtils.setDisplayName(new ItemStack(Material.TNT, 2), "�cBombe");
		bombs = ItemUtils.setLore(bombs, Arrays.asList("�aExplodieren wenn man", "�a sie setzt!", "�a"
				+ this.prices.get("tnts") + " Punkte"));
		shopInventory.addItem(bombs);
		
		ItemStack explosion2 = ItemUtils.setDisplayName(new ItemStack(Material.ENDER_PEARL), "�3Starke Explosion");
		explosion2 = ItemUtils.setLore(explosion2, Arrays.asList("�aErzeugt eine", "�6 starke Explosion", "�a"
				+ this.prices.get("enderpearl") + " Punkte"));
		shopInventory.addItem(explosion2);
		
		ItemStack ironarmor = ItemUtils.setDisplayName(new ItemStack(Material.IRON_CHESTPLATE), "�7Eisen-R�stung");
		ironarmor = ItemUtils.setLore(ironarmor, Arrays.asList("�a"
				+ this.prices.get("ironarmor") + " Punkte"));
		shopInventory.addItem(ironarmor);
		
		ItemStack diamondarmor = ItemUtils.setDisplayName(new ItemStack(Material.DIAMOND_CHESTPLATE), "�3Diamant-R�stung");
		diamondarmor = ItemUtils.setLore(diamondarmor, Arrays.asList("�a"
				+ this.prices.get("diamondarmor") + " Punkte"));
		shopInventory.addItem(diamondarmor);
		
		ItemStack magmaBall = ItemUtils.setDisplayName(new ItemStack(Material.DIAMOND_CHESTPLATE), "�6Resistenz-Ball");
		magmaBall = ItemUtils.setLore(magmaBall, Arrays.asList("�aRechtsklick -> �e30 Sekunden Resistenz 3", "�a"
				+ this.prices.get("magmaball") + " Punkte"));
		shopInventory.addItem(diamondarmor);
		
		if(gameplay && getConfig().getConfigurationSection("maps").getKeys(false).size() == 0) {
			System.out.println("[SW] GamePlay wird deaktiviert, da keine Maps gefunden wurden!");
			gameplay = false;
		}
		
		if(gameplay) {
			this.ingameScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
			ingameScoreboard.registerNewTeam("blue");
			ingameScoreboard.registerNewTeam("red");
			ingameScoreboard.getTeam("red").setPrefix("�c");
			ingameScoreboard.getTeam("red").setAllowFriendlyFire(true);
			ingameScoreboard.getTeam("blue").setPrefix("�9");
			ingameScoreboard.getTeam("blue").setAllowFriendlyFire(true);
			
			this.voteScoreboard = new ConfigurableScoreboard("�aVoting", true);
			
			int i = 1;
			for(String key : getConfig().getConfigurationSection("maps").getKeys(false)) {
				this.voteMaps.put(i, new VoteMap(key, i));
				this.voteScoreboard.setLine(i - 1, "�c" + key, 0);
				i++;
			}
			
			new SWCountdown();
			
			for(Player player : Bukkit.getOnlinePlayers()) {
				Bukkit.getPluginManager().callEvent(new PlayerJoinEvent(player, ""));
			}
		}
	}
	
	public void saveConfig() {
		cfgUtils.saveConfig();
	}
	
	public static enum GameState {
		WAITING, INGAME, RESTARTING;
	}
	
	public FileConfiguration getConfig() {
		return cfgUtils.getConfig();
	}
	
	public static SpeedWar getPlugin() {
		return instance;
	}
	
	private static SpeedWar instance;
	
}
